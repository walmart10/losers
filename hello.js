$(document).ready(function(){
    //reset page
    $('#picked').text("No One");
    var pickedWho = null;

    //selector
    $('#trenton').click(function(){
        pickedWho = 'Trenton';
        $('#picked').text(pickedWho);
    });
    $('#max').click(function(){
        pickedWho = 'Max';
        $('#picked').text(pickedWho);
    });
    $('#corbin').click(function(){
        pickedWho = 'Corbin';
        $('#picked').text(pickedWho);
    });
    $('#tgay').click(function(){
        pickedWho = 'TJ';
        $('#picked').text(pickedWho);
    });
    $('#xander').click(function(){
        pickedWho = 'Xander';
        $('#picked').text(pickedWho);
    });
    $('#reset').click(function(){
        pickedWho = 'No One';
        $('#picked').text(pickedWho);
    });

    //database
    $('#refresh').click(function(){
        var tr;
        $.getJSON('data.json', function(data) {
            $('table tr.data').remove();
            for (var i = 0; i < data.length; i++) {
                tr = $('<tr class="data" />');
                tr.append("<td>" + data[i][0] + "</td>");
                tr.append("<td>" + data[i][1] + "</td>");
                tr.append("<td>" + data[i][2] + "</td>");
                $('table').append(tr);
            }
        });
    });

    $('#update').click(function(){
        $.ajax({
            url: "",
            context: document.body
        }).done(function(data) {
            console.log(data);
        });
    });
});